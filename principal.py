#! /usr/bin/env python
import os, random, sys, math

import pygame
from pygame.locals import *

from configuracion import *
from funcionesRESUELTAS import *

from extras import *

#Funcion principal
def main():
        #Centrar la ventana y despues inicializar pygame
        os.environ["SDL_VIDEO_CENTERED"] = "1"
        pygame.init()
        #pygame.mixer.init()

        #Preparar la ventana
        pygame.display.set_caption("Pasa palabra...")
        screen = pygame.display.set_mode((ANCHO, ALTO))

        #tiempo total del juego
        gameClock = pygame.time.Clock()
        totaltime = 0
        segundos = TIEMPO_MAX
        fps = FPS_inicial

        puntos = 0
        candidata = ""
        listaFrases=[]
        listaPalabrasDiccionario=[]

        archivo= open("Matrix1999.srt","r")
        archivo2= open ("lemario.txt","r")
        #lectura del diccionario
        lectura2(archivo2, listaPalabrasDiccionario)

        #lectura del archivo. Cada linea es una frase
        lectura(archivo, listaFrases)

        #cerramos ambos archivos, ya que no se utilizaran mas
        archivo.close()
        archivo2.close()

        #quitamos los espacios de las listas para que queden completamente limpias
        listaFrases = eliminarVacios(listaFrases)
        listaPalabrasDiccionario = eliminarVacios(listaPalabrasDiccionario)

        #de cada frase elige la palabra mas larga
        listaPalabrasPeli = frasesToPalabras(listaFrases)

        #elige una al azar
        palabraActual=nuevaPalabra(listaPalabrasPeli)

        oculta=seleccionDeLetras(palabraActual)

        dibujar(screen, candidata, palabraActual, oculta, puntos,segundos)

        while segundos > fps/1000:
        # 1 frame cada 1/fps segundos
            gameClock.tick(fps)
            totaltime += gameClock.get_time()

            if True:
            	fps = 3

            #Buscar la tecla apretada del modulo de eventos de pygame
            for e in pygame.event.get():

                #QUIT es apretar la X en la ventana
                if e.type == QUIT:
                    pygame.quit()
                    return()

                #Ver si fue apretada alguna tecla
                if e.type == KEYDOWN:
                    letra = dameLetraApretada(e.key)
                    candidata += letra
                    if e.key == K_BACKSPACE:
                        candidata = candidata[0:len(candidata)-1]
                    if e.key == K_RETURN:
                        puntos += procesar(candidata, oculta, palabraActual, listaPalabrasDiccionario)
                        palabraActual=nuevaPalabra(listaPalabrasPeli)
                        oculta=seleccionDeLetras(palabraActual)
                        candidata = ""

            segundos = TIEMPO_MAX - pygame.time.get_ticks()/1000

            #Limpiar pantalla anterior
            screen.fill(COLOR_FONDO)

            #Dibujar de nuevo todo
            dibujar(screen, candidata, palabraActual, oculta, puntos,segundos)

            pygame.display.flip()

        while 1:
            #Esperar el QUIT del usuario
            for e in pygame.event.get():
                if e.type == QUIT:
                    pygame.quit()
                    return

#Programa Principal ejecuta Main
if __name__ == "__main__":
    main()
