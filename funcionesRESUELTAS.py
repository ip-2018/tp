from principal import *
from configuracion import *

import random
import math
import string

#funcion extra para quitar los espacios y los vacios en una lista
def eliminarVacios(lista):
    listaFinal = []
    for i in range(len(lista)):
        if lista[i] != "" and lista[i] != "  " and lista[i] != "   " and lista[i] != "    ":
            listaFinal.append(lista[i])
    return listaFinal

def elegirLaMasLarga(listaPalabras):
    palabraMasLarga = ""
    for i in range (len(listaPalabras)):
        if len(listaPalabras[i]) > len(palabraMasLarga):
            palabraMasLarga = listaPalabras[i]
    return palabraMasLarga

def frasesToPalabras(listaFrases):
    listaPalabras = []
    listaFinal = []
    for i in range (len(listaFrases)):
        listaPalabras.append(listaFrases[i].split(" "))
    for j in range(len(listaPalabras)):
        listaFinal.append(elegirLaMasLarga(listaPalabras[j]))
    return listaFinal

def quitarCaracteresEspeciales(linea):
    lineaMinusculas = linea.lower()
    lineaFinal = ""
    for caracter in lineaMinusculas:
        if caracter == "á":
            lineaFinal = lineaFinal + "a"
        elif caracter == "é":
            lineaFinal = lineaFinal + "e"
        elif caracter == "í":
            lineaFinal = lineaFinal + "i"
        elif caracter == "ó":
            lineaFinal = lineaFinal + "o"
        elif caracter == "ú":
            lineaFinal = lineaFinal + "u"
        elif (caracter == "0" or caracter == "1" or caracter == "2" or caracter == "3" or caracter == "4" or caracter == "5" or caracter == "6" or caracter == "7" or caracter == "8"
            or caracter == "9" or caracter == "," or caracter == "." or caracter == "?" or caracter == "¿" or caracter == "!" or caracter == "¡" or caracter == "-" or caracter == ">"
            or caracter == ">" or caracter == ":" or caracter == ";" or caracter == "º" or caracter == "%" or caracter == "$" or caracter == "#" or caracter == "*" or caracter == "&"
            or caracter == "_" or caracter == "=" or caracter == "+" or caracter == "(" or caracter == ")" or caracter == "[" or caracter == "]" or caracter == "{" or caracter == "}"
            or caracter == "\n"):
            lineaFinal = lineaFinal + ""
        else:
            lineaFinal = lineaFinal + caracter
    return lineaFinal

def lectura(archivo, listaFrases):
    #lineas arranca en 2 para que nos tome tambien la primera y la ultima
    lineas = 2
    for linea in archivo:
        lineas = lineas + 1
    archivo.seek(0)
    for j in range(lineas-1):
        listaFrases.append(archivo.readline())
    for k in range(len(listaFrases)):
        listaFrases[k] = quitarCaracteresEspeciales(listaFrases[k])

#otra funcion para leer un archivo porque son muy distintos
def lectura2(archivo2, listaPalabrasDiccionario):
    #lineas arranca en 2 para que nos tome tambien la primera y la ultima
    lineas = 2
    for linea in archivo2:
        lineas = lineas + 1
    archivo2.seek(0)
    for j in range(lineas-1):
        listaPalabrasDiccionario.append(archivo2.readline())
    for k in range(len(listaPalabrasDiccionario)):
        listaPalabrasDiccionario[k] = quitarCaracteresEspeciales(listaPalabrasDiccionario[k])

def seleccionDeLetras(palabra):
    posiciones = []
    oculta = list(palabra)
    i = 0
    #itera la 'mitad de la longitud de la palabra' veces
    while i < len(palabra)//2:
        n = random.randint(0, len(palabra)-1)
        #si la lista esta vacia, agregamos n
        if posiciones == []:
            posiciones.append(n)
            i = i + 1
        #si la lista no esta vacia nos fijamos si n ya esta en la misma, y si no esta lo agregamos
        else:
            cont = 0
            for numero in posiciones:
                if n != numero:
                    cont = cont + 1
            if cont == len(posiciones):
                posiciones.append(n)
                i = i + 1
                cont = 0
    #reemplazamos por un asterisco en las posiciones que tenemos guardadas en la lista de posiciones
    for posicion in posiciones:
        oculta[posicion] = "*"
    #juntamos las letras de cada posicion de la lista para obtener un string
    oculta = ''.join(oculta)
    return oculta

def nuevaPalabra(palabras):
    n = random.randint(0, len(palabras)-1)
    return palabras[n]

def esValida(candidata, oculta, palabra):
    #si la longitud de la candidata es igual a la de la oculta
    if len(candidata) == len(palabra):
        #nos fijamos que las letras descubiertas de la oculta coincidan con las de la ingresada
        coincidencias = 0
        letrasDescubiertas = len(palabra)
        for i in range(len(candidata)):
            if oculta[i] == "*":
                letrasDescubiertas = letrasDescubiertas - 1
            elif candidata[i] == oculta[i]:
                coincidencias = coincidencias + 1
        if letrasDescubiertas == coincidencias:
            return True
        else:
            return False
    else:
        return False

def Puntos(palabra):
    return len(palabra)

def procesar(candidata, oculta, palabra, listaPalabrasDiccionario):
    puntos = 0
    if esValida(candidata, oculta, palabra) == True:
        if candidata == palabra:
            puntos = puntos + (Puntos(palabra)*5)
        else:
            cont = 0
            for elemento in listaPalabrasDiccionario:
                if candidata == elemento:
                    puntos = puntos + Puntos(palabra)
                else:
                    cont = cont + 1
            if cont == len(listaPalabrasDiccionario):
                puntos = puntos - Puntos(palabra)
    else:
        puntos = puntos - Puntos(palabra)
    return puntos